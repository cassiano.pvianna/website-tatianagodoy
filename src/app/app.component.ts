import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { AboutModule } from './about/about.module';
import { CarouselModule } from './carousel/carousel.module';
import { AppTitleModule } from './components/app-title/app-title.module';
import { HeaderModule } from './header/header.module';
import { WelcomeHeroModule } from './welcome-hero/welcome-hero.module';
import { AppButtonModule } from './components/app-button/app-button.module';
import { WhatIsModule } from './what-is/what-is.module';
import { MethodModule } from './method/method.module';
import { EbookDownloadCardModule } from './ebook-download-card/ebook-download-card.module';
import { CallFixedIconModule } from './call-fixed-icon/call-fixed-icon.module';
import { MailFormModule } from './components/mail-form/mail-form.module';
import { VideoModule } from './components/video/video.module';
import { CallService } from './call-service.service';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    CommonModule,
    RouterOutlet,
    HeaderModule,
    WelcomeHeroModule,
    AboutModule,
    CarouselModule,
    AppTitleModule,
    AppButtonModule,
    WhatIsModule,
    MethodModule,
    EbookDownloadCardModule,
    CallFixedIconModule,
    MailFormModule,
    VideoModule
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  title = 'website-endometriose';

  constructor(private callService:CallService){

  }

  openMessageApp(){
    this.callService.openWhatsApp()
  }
}
