import { Component } from '@angular/core';

@Component({
  selector: 'app-method',
  templateUrl: './method.component.html',
  styleUrl: './method.component.scss',
})
export class MethodComponent {
  list = [
    {
      title: 'Empatia com a sua dor',
      text: `Eu mesma tenho endometriose e sei o quanto
      isso nos afeta. Comigo você terá um olhar sem julgamentos,
      que sofremos de alguns profissionais. Você terá uma abordagem
      leve e efetiva, considerando as limitações impostas pela endometriose e adenomiose.`,
    },
    {
      title: 'Abordagem personalizada',
      text: `Cada mulher é única, e compreendo
      que a endometriose e a adenomiose
      se manifestam de formas diferentes.
      Meu método inicia com uma avaliação
      minuciosa, considerando não apenas
      os sintomas, mas também a história
      de vida, necessidades específicas e
      objetivos individuais.`,
    },
    {
      title: 'Integração com profissionais de saúde',
      text: `Reconheço a importância da abordagem
       multidisciplinar no tratamento da endometriose
       e adenomiose. Fui convidada a integrar a equipe
       do Espaco Mínima pela querida Juliane Pedrini
       e pelo meu cirurgião Dr. Roger Berçot.
       Essa colaboração é crucial para
       oferecer um cuidado abrangente e eficaz.`,
    },
    {
      title: 'Empoderamento por meio da educação',
      text: `Acredito que o conhecimento é uma ferramenta poderosa.
       Vamos explorar juntas informações relevantes sobre nutrição,
       estilo de vida e autocuidado, capacitando você a tomar decisões
       informadas para promover sua saúde.`,
    },
    {
      title: 'Apoio emocional',
      text: `Compreendo que viver com endometriose e adenomiose
      é emocionalmente desafiador. Por isso, ofereço um ambiente de apoio,
      encorajamento e compreensão, promovendo o equilíbrio não apenas no corpo,
      mas também na mente e no espírito.`,
    },
  ];
}
