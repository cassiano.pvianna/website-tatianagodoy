import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MethodComponent } from './method.component';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  declarations: [MethodComponent],
  imports: [ComponentsModule, CommonModule],
  exports: [MethodComponent],
})
export class MethodModule {}
