import { NgModule } from '@angular/core';
import { WelcomeHeroComponent } from './welcome-hero.component';

@NgModule({
  declarations: [WelcomeHeroComponent],
  exports: [WelcomeHeroComponent],
})
export class WelcomeHeroModule {}
