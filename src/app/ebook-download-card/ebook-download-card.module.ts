import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EbookDownloadCardComponent } from './ebook-download-card.component';

@NgModule({
  declarations: [EbookDownloadCardComponent],
  imports: [CommonModule],
  exports: [EbookDownloadCardComponent],
})
export class EbookDownloadCardModule {}
