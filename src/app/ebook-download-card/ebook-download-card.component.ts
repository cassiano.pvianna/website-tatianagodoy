import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ebook-download-card',
  templateUrl: './ebook-download-card.component.html',
  styleUrls: ['./ebook-download-card.component.scss'],
})
export class EbookDownloadCardComponent implements OnInit {
  visible = true;

  constructor() {}

  ngOnInit() {}

  close() {
    this.visible = false;
  }
}
