import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CallService {
  constructor() {}

  openWhatsApp() {
    window.open(
      'https://api.whatsapp.com/send?phone=555499277050&text=Oi! Quero mais informações sobre a consulta.',
      '_self'
    );
  }

  openInstagram() {
    window.open(
      'https://www.instagram.com/nutritatianagodoy',
      '_self'
    );
  }
}
