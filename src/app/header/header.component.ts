import { Component, OnInit } from '@angular/core';
import { CallService } from '../call-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  constructor(private callService: CallService) {}

  ngOnInit() {}

  openInstagram() {
    this.callService.openInstagram();
  }
}
