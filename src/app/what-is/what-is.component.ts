import { Component } from '@angular/core';
import { CallService } from '../call-service.service';

@Component({
  selector: 'app-what-is',
  templateUrl: './what-is.component.html',
  styleUrl: './what-is.component.scss',
})
export class WhatIsComponent {

  constructor(private callService:CallService){

  }

  openMessageApp() {
    this.callService.openWhatsApp();
  }
}
