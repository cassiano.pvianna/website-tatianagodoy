import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WhatIsComponent } from './what-is.component';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  declarations: [WhatIsComponent],
  imports: [CommonModule, ComponentsModule],
  exports: [WhatIsComponent],
})
export class WhatIsModule {}
