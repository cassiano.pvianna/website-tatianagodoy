import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements OnInit {
  looping = true;
  loopInterval = 7000;
  index = 0;
  progressIndicatorClass = '';

  path = 'assets/images/depoimentos/';

  imgs: { src: string }[] = [];
  currentImg: any;

  loop!: NodeJS.Timeout;

  constructor() {}

  ngOnInit() {
    try {
      for (let i = 1; i <= 14; i++) {
        this.imgs.push({ src: this.path + 'depoimento_' + i + '.jpeg' });
      }
      this.currentImg = this.imgs[this.index];
      if (document != null) {
        this.scrollRight();
        this.startLooping();
      }
    } catch (e) {}
  }

  startLooping() {
    this.looping = true;
    this.scrollInLoop();
  }

  stopLooping() {
    this.looping = false;
    clearInterval(this.loop);
  }

  scrollInLoop() {
    if (!this.looping) return;
    this.loop = setInterval(() => {
      this.scrollRight();
    }, this.loopInterval);
  }

  scrollLeft() {
    this.index--;
    if (this.index < 0) {
      this.index = this.imgs.length - 1;
    }
    this.updateCurrentImg();
  }

  scrollRight() {
    this.index++;
    if (this.index >= this.imgs.length) {
      this.index = 0;
    }
    this.updateCurrentImg();
  }

  updateCurrentImg() {
    this.currentImg = this.imgs[this.index];
  }
}
