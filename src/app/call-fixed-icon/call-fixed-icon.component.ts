import { Component, OnInit } from '@angular/core';
import { CallService as CallService } from '../call-service.service';

@Component({
  selector: 'app-call-fixed-icon',
  templateUrl: './call-fixed-icon.component.html',
  styleUrl: './call-fixed-icon.component.scss',
})
export class CallFixedIconComponent implements OnInit {
  btnClass = 'whatsapp-color-icon first-position';
  visible = false;

  constructor(private callService: CallService) {}

  openWhatsApp() {
    this.callService.openWhatsApp();
  }

  ngOnInit(): void {
    this.visible = true;
    setTimeout(() => {
      this.btnClass = 'whatsapp-color-icon';
    }, 3000);
  }
}
