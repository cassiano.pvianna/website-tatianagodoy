import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CallFixedIconComponent } from './call-fixed-icon.component';

@NgModule({
  declarations: [CallFixedIconComponent],
  imports: [CommonModule],
  exports: [CallFixedIconComponent],
})
export class CallFixedIconModule {}
