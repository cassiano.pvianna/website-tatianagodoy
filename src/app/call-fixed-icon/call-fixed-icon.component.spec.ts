import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CallFixedIconComponent } from './call-fixed-icon.component';

describe('CallFixedIconComponent', () => {
  let component: CallFixedIconComponent;
  let fixture: ComponentFixture<CallFixedIconComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CallFixedIconComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CallFixedIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
