import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  afterNextRender,
} from '@angular/core';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrl: './video.component.scss',
})
export class VideoComponent {
  windowWidth = 200;
  playing = false;

  @ViewChild('videoElement') video!: ElementRef;
  @ViewChild('videoContainer') videoContainer!: ElementRef;

  constructor() {
    afterNextRender(() => {
      window.addEventListener('resize', () => {
        this.resize();
      });
      this.resize();

      window.addEventListener('scroll', (event) => {
        this.play();
      });
    });
  }

  resize() {
    setTimeout(() => {
      this.windowWidth = this.videoContainer.nativeElement.clientWidth - 40;
    }, 10);
  }

  play() {
    const iPhone = this.getMobileOS() == 'iOS';

    const video = this.video.nativeElement;

    const fraction = 0.8;
    let x = video.offsetLeft,
      y = video.offsetTop,
      w = video.offsetWidth,
      h = video.offsetHeight,
      r = x + w, //right
      b = y + h, //bottom
      visibleX,
      visibleY,
      visible;

    visibleX = Math.max(
      0,
      Math.min(w, window.scrollX + window.innerWidth - x, r - window.scrollX)
    );
    visibleY = Math.max(
      0,
      Math.min(h, window.scrollY + window.innerHeight - y, b - window.scrollY)
    );

    visible = (visibleX * visibleY) / (w * h);

    if (visible > fraction) {
      if (this.playing || iPhone) {
        return;
      }
      video.play();
    } else {
      this.playing = false;
      video.pause();
    }
  }

  getMobileOS() {
    const ua = navigator.userAgent;
    if (/android/i.test(ua)) {
      return 'Android';
    } else if (
      /iPad|iPhone|iPod/.test(ua) ||
      (navigator.platform === 'MacIntel' && navigator.maxTouchPoints > 1)
    ) {
      return 'iOS';
    }
    return 'Other';
  }
}
