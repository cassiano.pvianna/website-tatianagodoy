import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MailFormComponent } from './mail-form.component';

@NgModule({
  declarations: [MailFormComponent],
  imports: [CommonModule],
  exports: [MailFormComponent],
})
export class MailFormModule {}
