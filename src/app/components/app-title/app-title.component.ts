import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-title',
  templateUrl: './app-title.component.html',
  styleUrl: './app-title.component.scss',
})
export class AppTitleComponent {
  @Input() sub1?: string;
  @Input() sub2?: string;
  @Input() variation: 'white-centered' | 'primary-centered' | 'normal' = 'normal';
}
