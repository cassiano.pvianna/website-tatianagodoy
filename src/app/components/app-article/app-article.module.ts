import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppArticleComponent } from './app-article.component';

@NgModule({
  declarations: [AppArticleComponent],
  imports: [CommonModule],
  exports: [AppArticleComponent],
})
export class AppArticleModule {}
