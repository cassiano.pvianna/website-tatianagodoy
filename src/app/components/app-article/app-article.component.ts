import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-article',
  templateUrl: './app-article.component.html',
  styleUrl: './app-article.component.scss',
})
export class AppArticleComponent {
  @Input() variation: 'light' | 'dark' = 'light';
}
