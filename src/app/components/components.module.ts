import { AppButtonModule } from './app-button/app-button.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppArticleModule } from './app-article/app-article.module';
import { AppTitleModule } from './app-title/app-title.module';

const modules = [AppTitleModule, AppArticleModule, AppButtonModule];

@NgModule({
  imports: [CommonModule, ...modules],
  exports: modules,
})
export class ComponentsModule {}
