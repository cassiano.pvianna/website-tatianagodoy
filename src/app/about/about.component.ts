import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {
  anoAtual: number = 0;

  constructor() {}

  ngOnInit() {
    this.anoAtual = new Date().getFullYear();
  }
}
